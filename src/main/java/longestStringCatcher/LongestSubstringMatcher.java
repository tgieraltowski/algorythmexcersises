package longestStringCatcher;

import java.util.HashSet;
import java.util.Iterator;

public class LongestSubstringMatcher {
    public static void main(String[] args) {
        String s1= "AAABICCARIUSBBAAC";
        String s2= "ABAABBCCAAA";

        findLongestCommonSubstring(s1, s2);
        findLongestCommonSubstringWithIterator(s1, s2);
    }

    private static void findLongestCommonSubstring(String s1, String s2) {
        HashSet<String> allSubstrings = new HashSet<>();
        if(s2.length() >= s1.length()) {
            for (int i = 0; i < s1.length(); i++) {
                for (int j = i; j < s1.length(); j++) {
                    String string = s1.substring(i, j);
                    allSubstrings.add(string);
                }
            }
            String longestCommonSubstring = "";
            for (String string : allSubstrings) {
                if (string.length() > longestCommonSubstring.length() && s2.contains(string)) {
                    longestCommonSubstring = string;
                }
            }
            System.out.println(longestCommonSubstring);
        } else {
            for (int i = 0; i < s2.length(); i++) {
                for (int j = i; j < s2.length(); j++) {
                    String string = s2.substring(i, j);
                    allSubstrings.add(string);
                }
            }
            String longestCommonSubstring = "";
            for (String string : allSubstrings) {
                if (string.length() > longestCommonSubstring.length() && s1.contains(string)) {
                    longestCommonSubstring = string;
                }
            }
            System.out.println(longestCommonSubstring);
        }
    }

    private static void findLongestCommonSubstringWithIterator(String s1, String s2) {
        HashSet<String> allSubstrings = new HashSet<>();
        if(s2.length() >= s1.length()) {
            for (int i = 0; i < s1.length(); i++) {
                for (int j = i; j < s1.length(); j++) {
                    String string = s1.substring(i, j);
                    allSubstrings.add(string);
                }
            }
            Iterator<String> iterator = allSubstrings.iterator();
            String longestCommonSubstring = "";
            while (iterator.hasNext()) {
                if (iterator.next().length() > longestCommonSubstring.length() && s2.contains(iterator.next())) {
                    longestCommonSubstring = iterator.next();
                }
            }
            System.out.println(longestCommonSubstring);
        } else {
            for (int i = 0; i < s2.length(); i++) {
                for (int j = i; j < s2.length(); j++) {
                    String string = s2.substring(i, j);
                    allSubstrings.add(string);
                }
            }
            Iterator<String> iterator = allSubstrings.iterator();
            String longestCommonSubstring = "";
            while (iterator.hasNext()) {
                if (iterator.next().length() > longestCommonSubstring.length() && s1.contains(iterator.next())) {
                    longestCommonSubstring = iterator.next();
                }
            }
            System.out.println(longestCommonSubstring);
        }
    }
}
