package efficientCashMachine;

import java.math.BigDecimal;
import java.util.Scanner;

public class EfficientCashMachine {
    public static void main(String[] args) {

        System.out.println("Welcome to THOMAS BANK ATM. How much cash would you like to withdraw?");
        BigDecimal cashToCollect = new Scanner(System.in).nextBigDecimal();
        dispenseCash(cashToCollect);

    }

    private static void dispenseCash(BigDecimal cashToCollect) {
        Money twoHundred$Bill = new Money(BigDecimal.valueOf(200), 0);
        Money oneHundred$Bill = new Money(BigDecimal.valueOf(100), 0);
        Money fifty$Bill = new Money(BigDecimal.valueOf(50), 0);
        Money twenty$Bill = new Money(BigDecimal.valueOf(20), 0);
        Money ten$Bill = new Money(BigDecimal.valueOf(10), 0);
        Money five$Bill = new Money(BigDecimal.valueOf(5), 0);
        Money two$Coin = new Money(BigDecimal.valueOf(2), 0);
        Money one$Coin = new Money(BigDecimal.valueOf(1), 0);
        Money half$Coin = new Money(BigDecimal.valueOf(0.5), 0);
        Money quarter$Coin = new Money(BigDecimal.valueOf(0.25), 0);
        Money tenCent$Coin = new Money(BigDecimal.valueOf(0.1), 0);
        Money fiveCent$Coin = new Money(BigDecimal.valueOf(0.05), 0);
        Money twoCent$Coin = new Money(BigDecimal.valueOf(0.02), 0);
        Money oneCent$Coin = new Money(BigDecimal.valueOf(0.01), 0);

        Money[] monies = {twoHundred$Bill, oneHundred$Bill, fifty$Bill, twenty$Bill, ten$Bill, five$Bill
                , two$Coin, one$Coin, half$Coin, quarter$Coin, tenCent$Coin, fiveCent$Coin, twoCent$Coin, oneCent$Coin};

        for (Money money : monies) {
            if (cashToCollect.compareTo(money.getValue()) >= 0) {
                do {
                    cashToCollect = cashToCollect.subtract(money.getValue());
                    money.setCount(money.getCount() + 1);
                } while (cashToCollect.divide(money.getValue()).compareTo(BigDecimal.ONE) >= 1);
            }
            System.out.println(money.getValue() + "$ : x " + money.getCount());
        }
    }
}
