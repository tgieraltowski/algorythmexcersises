package efficientCashMachine;

import java.math.BigDecimal;

public class Money {

    private BigDecimal value;
    private int count;

    public Money(BigDecimal value, int count) {
        this.value = value;
        this.count = count;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
