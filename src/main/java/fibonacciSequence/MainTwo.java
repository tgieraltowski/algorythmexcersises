package fibonacciSequence;

import java.util.Scanner;

public class MainTwo {
    static int number1 = 1;
    static int number2 = 1;
    static int number3 = 2;

    public static void main(String[] args) {



        System.out.println("How many elements should be shown? ");
        int numberOfElements = new Scanner(System.in).nextInt();

        showElements(numberOfElements);

        showElementsTwo(numberOfElements);

        showElements3(numberOfElements);

        showElements4(numberOfElements);

        showElements5(numberOfElements);

        fibonacciRecurent(numberOfElements);

    }

    private static void showElements(int numberOfElements) {
        for (int i = 1; i <= numberOfElements; i++) {
            System.out.print(i * 2 - 1 + " ");
        }
        System.out.println();
    }

    private static void showElementsTwo(int numberOfElements) {
        for (int i = 1; i <= numberOfElements; i++) {
            if (i % 2 != 0) {
                System.out.printf("%.0f ", (i / 2.0 + 0.5) * 100.0);
            } else {
                System.out.print(i / 2 * 10 + " ");
            }
        }
        System.out.println();
    }

    private static void showElements3(int numberOfElements) {
        boolean peakReached = false;

        do {
            for (int i = 0; i <= numberOfElements; i++) {
                if (peakReached == false) {
                    System.out.print(i * 2 + " ");
                    if (i == numberOfElements) {
                        peakReached = true;
                    }

                }
            }
        } while (!peakReached);
        if (peakReached) {
            for (int i = numberOfElements - 1; i >= 0; i--) {
                System.out.print(i * 2 + " ");
            }
        }
        System.out.println();
    }

    private static void showElements4(int numberOfElements) {
        for (int i = 1; i <=numberOfElements; i++) {
            System.out.printf("%.0f ", Math.pow(i, 2));
        }
        System.out.println();
    }

    private static void showElements5(int numberOfElements) {
        int number1 = 1;
        int number2 = 1;
        int number3 = 2;

        System.out.print(number1 + " " + number2 + " ");

        for (int i = 2; i < numberOfElements; i++) {
            number3 = number2 + number1;
            System.out.print(number3 + " ");
            number1 = number2;
            number2 = number3;

        }
        System.out.println();
    }

    private static void fibonacciRecurent(int numberOfElements) {

        if (numberOfElements > 0) {
            number3 = number2 + number1;
            System.out.print(number3 + " ");
            number1 = number2;
            number2 = number3;
            fibonacciRecurent(numberOfElements - 1);
        }

    }
}
