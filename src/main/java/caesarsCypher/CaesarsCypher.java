package caesarsCypher;

import java.util.Scanner;

public class CaesarsCypher {
    public static void main(String[] args) {

        System.out.println("Enter sentence to cypher: ");
        String sentence = new Scanner(System.in).nextLine();

        System.out.println(cypher(sentence));

        System.out.println(decipher("QLHSUCBMDFLHO MHVW EDUGCR EOLVNR"));
    }

    private static String cypher(String sentence) {
        char[] chars = sentence.toCharArray();
        StringBuilder sB = new StringBuilder();
        for (char ch : chars) {
            if (ch == ' ') {
                sB.append(ch);
            } else {
                int ascii = (int)ch;
                ascii += 3;
                if (ascii > 90 && ascii < 94) {
                    ascii = ascii - 26;
                }
                if (ascii > 122 && ascii < 126) {
                    ascii = ascii - 26;
                }
                ch = (char)ascii;
                sB.append(ch);
            }
        }
        return sB.toString();
    }

    private static String decipher(String sentence) {
        char[] chars = sentence.toCharArray();
        StringBuilder sB = new StringBuilder();
        for (char ch : chars) {
            if (ch == ' ') {
                sB.append(ch);
            } else {
                int ascii = (int)ch;
                ascii -= 3;
                if (ascii > 60 && ascii < 65) {
                    ascii = ascii + 26;
                }
                if (ascii > 92 && ascii < 97) {
                    ascii = ascii + 26;
                }
                ch = (char)ascii;
                sB.append(ch);
            }
        }
        return sB.toString();
    }

}
