package primeNumbersChecker;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter your number: ");
        int number = new Scanner(System.in).nextInt();

        System.out.println("Is number : " + number + " a prime number? : " + checkIfNumberIsPrime(number));

        }

    private static boolean checkIfNumberIsPrime(int number) {
       for (int i = 2; i < number/2 - 1; i++) {
           if (number % i == 0) {
               return false;
           }
       }
       return true;
    }

}

