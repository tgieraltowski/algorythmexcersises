package palindrom;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {

        System.out.println("Enter a sentence: ");
        String myString = new Scanner(System.in).nextLine();
        System.out.println("Is your sentence a palindrom? : "+ checkIfStringIsAPalindrom(myString));
    }

    private static boolean checkIfStringIsAPalindrom(String myString) {
        myString = myString.replace(" ", "").toUpperCase();
        char[] chars = myString.toCharArray();
        for (int i = 0; i < chars.length -1; i++) {
            if (chars[i] != chars[chars.length-1 - i]) {
                return false;
            }
        }
        return true;
    }
}
