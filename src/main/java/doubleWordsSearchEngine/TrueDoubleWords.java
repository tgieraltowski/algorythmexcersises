package doubleWordsSearchEngine;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class TrueDoubleWords {
    public static void main(String[] args) {

        System.out.println("Enter your string: ");
        String string = new Scanner(System.in).nextLine();
        showAllDoubleWords(string);
    }

    private static void showAllDoubleWords(String string) {
        Set<String> allSubstrings = new HashSet<>();
        for (int i = 0; i < string.length(); i++) {
            for (int j = i; j < string.length(); j++) {
                if (string.substring(i, j).length() > 1) {
                    allSubstrings.add(string.substring(i, j));
                }
            }
        }
        for (String substring : allSubstrings) {
            if (string.contains(substring.concat(substring))) {
                System.out.println(substring);
            }
        }
    }
}
