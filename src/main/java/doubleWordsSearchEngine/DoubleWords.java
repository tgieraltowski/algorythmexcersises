package doubleWordsSearchEngine;

import java.util.HashSet;
import java.util.Scanner;

public class DoubleWords {
    public static void main(String[] args) {

        String charactersString = new Scanner(System.in).nextLine();

        findDoubleWordsWithSB(charactersString);
    }

    private static void findDoubleWordsWithSB(String charactersString) {
        char[] chars = charactersString.toCharArray();
        HashSet<String> words = new HashSet<>();

        for (int x = 0; x <chars.length; x++) {
            StringBuilder sb = new StringBuilder();
            for (int i = x; i < chars.length; i++) {
                StringBuilder currentWord = new StringBuilder();
                for (int y = i; y < chars.length; y++) {
                    currentWord = currentWord.append(chars[y]);
                }
                if (currentWord.toString().contains(sb) && sb.length() > 1) {
                    words.add(sb.toString());
                    sb.append(chars[i]);
                } else {
                    sb.append(chars[i]);
                }
            }
        }
        System.out.println(words);
    }
}
