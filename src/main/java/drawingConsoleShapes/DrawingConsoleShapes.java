package drawingConsoleShapes;

public class DrawingConsoleShapes {
    public static void main(String[] args) {
        int[][] commandArray = {
                {1, 0, 0, 0, 0, 0},
                {1, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {1, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1},
        };

        drawTreeShape(commandArray);
        System.out.println();
        drawTreeShape2(6, 6);
        System.out.println();
        drawDiagonal(6, 6);
        System.out.println();
        drawOutline(6, 6);
        System.out.println();
        drawXShape(6, 6);
        System.out.println();
        drawReverseTree(6 ,6);
        System.out.println();
        drawSymmetricalTree(10, 10);
    }

    private static void drawTreeShape(int[][] commandArray) {

        for (int row = 0; row < commandArray.length; row++) {
            for (int column = 0; column < commandArray.length; column++) {
                if (commandArray[row][column] == 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void drawTreeShape2(int height, int width) {
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                if (row >= column) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void drawDiagonal(int height, int width) {
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                if (row == column) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void drawOutline(int height, int width) {
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                if (row == height-1 || column == height-1 || column == 0 || row == 0) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void drawXShape(int height, int width) {
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                if (row == column || row == (width - column -1)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void drawReverseTree(int height, int width) {
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                if (row <= column) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void drawSymmetricalTree(int height, int width) {
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                if (column >= (width-row)/2 && (width - column-1) >= (width-row)/2) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}


